package com.CPT202.PetGroomingSystem.sevices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CPT202.PetGroomingSystem.models.Customer;
import com.CPT202.PetGroomingSystem.repositories.CustomerRepo;
import java.util.*;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepo customerRepo;

    public Customer addNewCustomer(Customer customer) {
        return customerRepo.save(customer);
    }

    public List<Customer> getCustomerList() {
        return customerRepo.findAll();
    }
}
