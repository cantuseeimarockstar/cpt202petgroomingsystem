package com.CPT202.PetGroomingSystem.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.engine.AttributeName;

import com.CPT202.PetGroomingSystem.models.Customer;
import com.CPT202.PetGroomingSystem.repositories.CustomerRepo;
import com.CPT202.PetGroomingSystem.sevices.CustomerService;

@Controller
// Restful api:會返回對象
@RequestMapping(value = "/admin/maintaincustomerfile")
public class CustomerFileController {
    @Autowired
    private CustomerService customerService;

    // localhost:8080/admin/maintaincustomerfile/customerfile
    @GetMapping("/customerfile")
    public String getList(Model model) {
        model.addAttribute("customerlist", customerService.getCustomerList());
        return "allCustomers";
    }

    @GetMapping("/addnewcustomer")
    public String addNewCustomer(Model model) {
        model.addAttribute("customer", new Customer());
        return "addNewCustomer";
    }

    @PostMapping("/addnewcustomer")
    public String confirmNewCustomer(@ModelAttribute("customer") Customer customer) {
        // 其實在這兒會加一個新的顧客
        customerService.addNewCustomer(customer);

        return "home";
    }

}
