package com.CPT202.PetGroomingSystem.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.CPT202.PetGroomingSystem.models.Groomer;

@RestController
@ResponseBody
@RequestMapping(value = "/admin/maintainGroomerFile")
public class GroomerFlieController {

    private List<Groomer> groomers = new ArrayList<Groomer>();

    @GetMapping("/groomerfile")
    public String getList() {
        return "A list of groomers";
    }

    @PostMapping("/addnewgroomer")
    public void addNewCustomer(@RequestBody Groomer groomer) {
        groomers.add(groomer);
        ResponseEntity.ok("Successfully!");
    }

    // {"customerName":"Penomeco","contactInfo":"+32 0223546847"}
    @PostMapping("/test")
    public String test() {
        return "een list van nummers";
    }

}
