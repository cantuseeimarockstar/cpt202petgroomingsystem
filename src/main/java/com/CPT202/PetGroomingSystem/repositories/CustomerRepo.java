package com.CPT202.PetGroomingSystem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.CPT202.PetGroomingSystem.models.Customer;

public interface CustomerRepo extends JpaRepository<Customer, Integer> {

}
