package com.CPT202.PetGroomingSystem.models;

public class Groomer {
    private String groomerName;
    private String contactInfo;

    public Groomer() {
    }

    public Groomer(String groomerName, String contactInfo) {
        this.groomerName = groomerName;
        this.contactInfo = contactInfo;
    }

    public String getGroomerName() {
        return groomerName;
    }

    public void setGroomerName(String groomerName) {
        this.groomerName = groomerName;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

}
